#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "core.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void addMessage(const QString & m);

private slots:
    void on_btn_send_clicked();

    void on_btn_add_friend_clicked();


    void on_btn_name_clicked();

public:
    void setInputOK(bool flag = true);

private:
    Ui::MainWindow *ui;
    core *core_;
};
#endif // MAINWINDOW_H
