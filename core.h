#ifndef CORE_H
#define CORE_H

#include <tox/tox.h>
#include <sodium.h>
#include <QString>
#include <QtConcurrent/QtConcurrent>

typedef struct DHT_node
{
    const char *ip;
    uint16_t port;
    const char key_hex[TOX_PUBLIC_KEY_SIZE * 2 + 1]; // 1 for null terminator
} DHT_node;

const static DHT_node nodes[] = {
    {"218.28.170.22", 33445, "DBACB7D3F53693498398E6B46EF0C063A4656EB02FEFA11D72A60BAFA8DF7B59"},
    {"node.tox.biribiri.org", 33445, "F404ABAA1C99A9D37D61AB54898F56793E1DEF8BD46B1038B9D822E8460FAB67"},
    {"128.199.199.197", 33445, "B05C8869DBB4EDDD308F43C1A974A20A725A36EACCA123862FDE9945BF9D3E09"},
    {"2400:6180:0:d0::17a:a001", 33445, "B05C8869DBB4EDDD308F43C1A974A20A725A36EACCA123862FDE9945BF9D3E09"},
    {"85.143.221.42", 33445, "DA4E4ED4B697F2E9B000EEFE3A34B554ACD3F45F5C96EAEA2516DD7FF9AF7B43"},
    {"2a04:ac00:1:9f00:5054:ff:fe01:becd", 33445, "DA4E4ED4B697F2E9B000EEFE3A34B554ACD3F45F5C96EAEA2516DD7FF9AF7B43"},
    {"78.46.73.141", 33445, "02807CF4F8BB8FB390CC3794BDF1E8449E9A8392C5D3F2200019DA9F1E812E46"},
    {"2a01:4f8:120:4091::3", 33445, "02807CF4F8BB8FB390CC3794BDF1E8449E9A8392C5D3F2200019DA9F1E812E46"},
    {"tox.initramfs.io", 33445, "3F0A45A268367C1BEA652F258C85F4A66DA76BCAA667A49E770BCC4917AB6A25"},
    {"tox2.abilinski.com", 33445, "7A6098B590BDC73F9723FC59F82B3F9085A64D1B213AAF8E610FD351930D052D"},
    {"205.185.115.131", 53, "3091C6BEB2A993F1C6300C16549FABA67098FF3D62C6D253828B531470B53D68"},
    {"tox.kurnevsky.net", 33445, "82EF82BA33445A1F91A7DB27189ECFC0C013E06E3DA71F588ED692BED625EC23"}};

class MainWindow;

class core : public QObject
{
    Q_OBJECT
public:
    core(MainWindow * mainwindow);

signals:
    void SetMainWindowOutput(const QString& m);

public:
    bool set_name(const char *name = "toxchat");
    static void handle_friend_message(
            Tox *mytox, uint32_t friend_number, TOX_MESSAGE_TYPE type,
            const uint8_t *message, size_t length,
            void *user_data);
    static void handle_friend_request(
            Tox *mytox, const uint8_t *public_key, const uint8_t *message, size_t length,
            void *user_data);
    static void handle_connection_status(
            Tox *tox, TOX_CONNECTION connection_status, void *user_data);
    static void handle_friend_connection_status(
            Tox *tox, uint32_t friend_number, TOX_CONNECTION connection_status,
                    void *user_data);
    void send_message(const QString& m);
    void add_friend(const QString& id);
    QString get_tox_id();
    QString get_pk();


private:
    void process();
    void DHT_Boostarp();

private:
    Tox *tox_;
    MainWindow *mainwindow_;
    bool wait_request_ = false;
};

#endif // CORE_H
