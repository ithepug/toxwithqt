#include "core.h"
#include "mainwindow.h"

core::core(MainWindow * mainwindow)
{
    mainwindow_ = mainwindow;
    TOX_ERR_NEW err_new;
    Tox_Err_Options_New err_options;
    struct Tox_Options *default_options = nullptr;
    default_options = tox_options_new(&err_options);
    default_options->log_user_data = this;
    tox_ = tox_new(default_options, &err_new);
    tox_callback_friend_request(tox_, &core::handle_friend_request);
    tox_callback_friend_message(tox_, &core::handle_friend_message);
    tox_callback_self_connection_status(tox_, &core::handle_connection_status);
    tox_callback_friend_connection_status(tox_, &core::handle_friend_connection_status);

    DHT_Boostarp();

    set_name("toxchat");

    QTimer *timer = new QTimer () ;
    connect(timer,&QTimer::timeout,this,&core::process);
    timer->start(1000);
    //    QtConcurrent::run(this, &core::DHT_Boostarp);
}

bool core::set_name(const char *name)
{
    TOX_ERR_SET_INFO err;
    return tox_self_set_name(tox_, (uint8_t *)name, sizeof(name), &err);
}

void core::handle_friend_message(Tox *tox, uint32_t friend_number, TOX_MESSAGE_TYPE type, const uint8_t *message, size_t length, void *user_data)
{
    TOX_ERR_FRIEND_QUERY name_err;
    uint8_t *friend_name = new u_int8_t[20];
    tox_friend_get_name(tox, friend_number, friend_name, &name_err);
    qDebug("%s: %s", friend_name, message);
    static_cast<core*>(user_data)->mainwindow_->
            addMessage(
                QString((char*)friend_name) 
                + ": " 
                + QString((char*)message));
}

void core::handle_friend_request(Tox *tox, const uint8_t *public_key, const uint8_t *message, size_t length, void *user_data)
{
    TOX_ERR_FRIEND_ADD err_friend_add;
    tox_friend_add_norequest(tox, public_key, &err_friend_add);
    TOX_ERR_FRIEND_QUERY name_err;
    uint8_t *friend_name = new u_int8_t[20];
    tox_friend_get_name(tox, 0, friend_name, &name_err);

    static_cast<core*>(user_data)->mainwindow_->
            addMessage(QString("=>") + "收到好友请求，已自动同意");
    static_cast<core*>(user_data)->mainwindow_->setInputOK();
    if (err_friend_add != TOX_ERR_FRIEND_ADD_OK)
    {
        fprintf(stderr, "unable to add friend: %d\n", err_friend_add);
    }
}

void core::handle_connection_status(Tox *tox, TOX_CONNECTION connection_status, void *user_data)
{
    switch (connection_status)
    {
    case TOX_CONNECTION_NONE:
        static_cast<core*>(user_data)->mainwindow_->addMessage("=>离线");
        break;
    case TOX_CONNECTION_TCP:
        static_cast<core*>(user_data)->mainwindow_->addMessage("=>TCP在线");
        break;
    case TOX_CONNECTION_UDP:
        static_cast<core*>(user_data)->mainwindow_->addMessage("=>UDP在线");
        break;
    }
}

void core::handle_friend_connection_status(Tox *tox, uint32_t friend_number, TOX_CONNECTION connection_status, void *user_data)
{
    if(static_cast<core*>(user_data)->wait_request_)
        static_cast<core*>(user_data)->mainwindow_->addMessage("=>对方已同意您的好友请求");
    else
        static_cast<core*>(user_data)->mainwindow_->addMessage("=>好友上线");
    static_cast<core*>(user_data)->mainwindow_->setInputOK();
}

void core::send_message(const QString& m)
{
    qDebug()<<m;
    TOX_ERR_FRIEND_SEND_MESSAGE err_send;
    if(tox_self_get_friend_list_size(tox_) > 0)
    {
        tox_friend_send_message(tox_, 0, 
                                TOX_MESSAGE_TYPE_NORMAL, 
                                (const uint8_t *)(m.toStdString().c_str()), m.size(),
                                &err_send);
        size_t name_size = tox_self_get_name_size(tox_);
        uint8_t *name = new uint8_t[name_size];
        tox_self_get_name(tox_,name);
        this->mainwindow_->addMessage(QString((char*)name) + ": " + m);
    }
}

void core::add_friend(const QString &id)
{
    TOX_ERR_FRIEND_ADD error_add;
    uint8_t message[] = "这是从ToxChat发出的好友请求";
    uint8_t address[38] = {0};
    QString id_lower = id.toLower();
    if(id_lower.size() == 76)
    {
        for(int i=0;i!=76;i++)
        {
            int n;
            n = id_lower[i] > '9' ? id_lower[i].toLatin1() - 'a' + 10: id_lower[i].toLatin1() - '0';
            address[i >> 1] |= n << 4*((i+1) % 2);
        }
        tox_friend_add(tox_, address, message, sizeof(message), &error_add);
        wait_request_ = true;
    }
    else {
        qDebug()<<"tox id error!";
    }
}

void core::DHT_Boostarp()
{
    //    emit SetMainWindowOutput("连接中...");
    mainwindow_->addMessage("=>连接中...");
    for (size_t i = 0; i < sizeof(nodes) / sizeof(DHT_node); i++)
    {
        unsigned char key_bin[TOX_PUBLIC_KEY_SIZE];
        sodium_hex2bin(key_bin, sizeof(key_bin), nodes[i].key_hex, sizeof(nodes[i].key_hex) - 1,
                       NULL, NULL, NULL);
        tox_bootstrap(tox_, nodes[i].ip, nodes[i].port, key_bin, NULL);
    }
}

QString core::get_tox_id()
{
    char h[77];
    u_int8_t buf[40];
    tox_self_get_address(tox_, buf);
    int j = 0;
    for (int i = 0; i != 38; i++)
    {
        int n = buf[i] >> 4;
        if (n < 10)
            h[j++] = n + '0';
        else
            h[j++] = n - 10 + 'a';
        n = buf[i] & 15;
        if (n < 10)
            h[j++] = n + '0';
        else
            h[j++] = n - 10 + 'a';
    }
    h[76] = '\0';
    return h;
}

QString core::get_pk()
{
    char h[65];
    u_int8_t buf[32];
    tox_self_get_secret_key(tox_, buf);
    int j = 0;
    for (int i = 0; i != 32; i++)
    {
        int n = buf[i] >> 4;
        if (n < 10)
            h[j++] = n + '0';
        else
            h[j++] = n - 10 + 'a';
        n = buf[i] & 15;
        if (n < 10)
            h[j++] = n + '0';
        else
            h[j++] = n - 10 + 'a';
    }
    h[64] = '\0';
    return h;
}

void core::process()
{
    tox_iterate(tox_, this);
}
