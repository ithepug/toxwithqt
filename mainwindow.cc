#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    core_ = new core (this);
    ui->le_tox_id->setText(core_->get_tox_id());
    this->setInputOK(false);
    connect(core_,&core::SetMainWindowOutput,
            this,&MainWindow::addMessage);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addMessage(const QString &m)
{
    ui->te_dialog->append(m);
}


void MainWindow::on_btn_send_clicked()
{
    QString m = ui->textEdit->toPlainText();
    ui->textEdit->clear();
    core_->send_message(m);
}

void MainWindow::on_btn_add_friend_clicked()
{
    this->addMessage("=>已发出请求，请等待回应...");
    QString f_id = ui->le_friend_tox_id->text();
    core_->add_friend(f_id);
}

void MainWindow::setInputOK(bool flag)
{
    ui->textEdit->setReadOnly(!flag);
    if(flag)
    {
        ui->textEdit->setTextColor(QColor(10,10,10));
        ui->textEdit->clear();
    }
    else
    {
        ui->textEdit->setTextColor(QColor(200,200,200));
        ui->textEdit->setText("请先添加朋友");
    }
}

void MainWindow::on_btn_name_clicked()
{
    core_->set_name(ui->le_name->text().toLatin1());
}
